from flask import Flask, url_for
from flask import json
from flask import request
from flask import Response

from evaluator import napovejPrimerA
from evaluator import napovejPrimerB
from evaluator import napovejPrimerC

app = Flask(__name__)

@app.route('/predict', methods = ["POST"])
def api_articles():
    #print("Request: " + json.dumps(request.json))
    
    data_json = json.loads(request.data)
    modelId = data_json['modelId']
    if modelId == 'A':
        ocena = napovejPrimerA(data_json['question'], data_json['questionResponse'])
    elif modelId == 'B':
        ocena = napovejPrimerB(data_json['question'], data_json['questionResponse'])
    elif modelId == 'C':
        ocena = napovejPrimerC(data_json['question'], data_json['questionResponse'])
    else:
        print("Napaka!")

    data = {
        "score": ocena,
        "probability": None
    }
    js = json.dumps(data)

    return Response(js, status=200, mimetype='application/json')


if __name__ == '__main__':
    app.run(port=8080)