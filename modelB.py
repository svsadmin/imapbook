# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 13:13:01 2019

@author: admin

"""

import pickle
import test
import napovedovanje
import gensim
import numpy
import matplotlib as plt
from sklearn.metrics import f1_score

vsi, delezi = test.nalozi(".\podatki\Weightless_dataset_train.csv")
u_, d = test.nalozi(".\podatki\Weightless_dataset_trainA.csv")

zgodba = test.naloziZgodbo(".\podatki\Weightless.txt")

u1A, u05A, u0A = test.pretvoriVslovar(u_)
#u1, u05, u0 = test.pretvoriVslovar(ucni)

besedisce = napovedovanje.vrniBesedisceA(u1A) + zgodba
besedisce = [napovedovanje.predelaj(b) for b in besedisce]
idf, tfidf_model = test.pridobiIdf([" ".join(b) for b in besedisce])

W2Vmodel = gensim.models.Word2Vec(
        besedisce,
        size=150,
        window=10,
        min_count=2,
        workers=10)
W2Vmodel.train(besedisce, total_examples=len(besedisce), epochs=10)

modeli = napovedovanje.Modeli()
modeli.tf_idf = tfidf_model
modeli.W2Vmodel = W2Vmodel
modeli.Idf_ = idf

def nauciModele(ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, vektorji05, modeli):
    n_modeli1 = {}
    n_modeli2 = {}
    maks_vrednosti = {}
    
    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "cossim")
        X2, y2 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "kontekst")
        maks_vrednosti["kontekst"] = max(X2)
        X2 = X2/max(X2)
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "w2vNajsim")
        maks_vrednosti["w2vNajsim"] = max(X3)
        X3 = X3/max(X3)
        X = numpy.concatenate((X, X3), axis=1)
        
        if vpr in vektorji05.keys():
            X1, y1 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji05, modeli, "cossim")
            X1 /= max(X1)
            maks_vrednosti["cossim05"] = max(X1)
            X = numpy.concatenate((X, X1), axis=1)
        
        n_modeli1[vpr] = napovedovanje.ridgeKlasifikator(X, y)
        n_modeli2[vpr] = napovedovanje.randomForest(X, y)
        
    return n_modeli1, n_modeli2, maks_vrednosti


def testiraj(testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, vektorji05, maks_vrednosti, modeli, n_modeli1, n_modeli2):
    napoved_ = []
    y_ = []
    rezultati = []
    
    f = plt.pyplot.figure()
    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "cossim")
        X2, y2 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "kontekst")
        X2 = X2/maks_vrednosti["kontekst"]
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "w2vNajsim")
        X3 = X3/maks_vrednosti["w2vNajsim"]
        X = numpy.concatenate((X, X3), axis=1)
        
        if vpr in vektorji05.keys():
            X1, y1 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji05, modeli, "cossim")
            X1 = X1/maks_vrednosti["cossim05"]
            X = numpy.concatenate((X, X1), axis=1)

        napoved1 = n_modeli1[vpr].predict(X)
        napoved2 = n_modeli2[vpr].predict(X)
        
        napoved = [test.randomDelezi(vpr, delezi, napoved1[i], napoved2[i]) for i in range(0, len(napoved2))]
        
        
        for i in range(0, len(y)):
            napoved_.append(napoved[i])
            y_.append(y[i])
        
        tocnost = [1 if y[i] == napoved[i] else 0 for i in range(0, len(y))]
        t = sum(tocnost)/len(tocnost)

        plt.pyplot.hist(X)
        
        
        tocnost = [1 if y[i] == napoved[i] else 0 for i in range(0, len(y))]
        t = sum(tocnost)/len(tocnost)
        fmakro = f1_score(y, napoved, average='macro')
        fmikro = f1_score(y, napoved, average='micro')
        
        rezultati.append((vpr, fmakro, fmikro))
    f.savefig("fB.pdf", bbox_inches='tight')
    
    return rezultati, napoved_, y_



#Vsi podatki
u1, u05, u0 = test.pretvoriVslovar(vsi)

vu1 = napovedovanje.vrniVektorje(u1)

vu05 = napovedovanje.vrniVektorje(u05)

n_modeli1, n_modeli2, maks_vrednosti = nauciModele(u1, u05, u0, vu1, vu05, modeli)

r, n, y = testiraj(u1, u05, u0, vu1, vu05, maks_vrednosti, modeli, n_modeli1, n_modeli2)

print(r)
print("F1 mikro: " + str(f1_score(y, n, average='micro')))
print("F1 makro: " + str(f1_score(y, n, average='macro')))

pickle.dump((n_modeli1, n_modeli2, maks_vrednosti, u1, vu1, vu05, delezi, modeli), open("modelB.p", "wb" ))



#napovedni_modeli = []
#nm, maks_vrednosti = nauciModele(u1, u05, u0, u1_vektorji, u05_vektorji, modeli)
#napovedni_modeli.append(nm)

def napovejPrimerB_(vprasanje, odgovor):
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}
    
    t_vpr[vprasanje] = v
    
    cosSim = napovedovanje.vrniCosSim(vprasanje, t_vpr, u1_vektorji, modeli)
    kontekst = napovedovanje.vrniPodobnostSKontekstom(vprasanje, t_vpr, u1_vektorji, modeli)
    kontekst /= maks_vrednosti["kontekst"]
    w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u1_vektorji, modeli)
    w2vNajSim /= maks_vrednosti["w2vNajsim"]
    
    x = [cosSim, kontekst, w2vNajSim]
    
    if vprasanje in u05_vektorji.keys():
        cosSim05 = napovedovanje.vrniCosSim(vprasanje, t_vpr, u05_vektorji, modeli)
        x.append(cosSim05)
    
    x = numpy.array(x).reshape(1, -1)
    
    napoved1 = napovedni_modeli1[vprasanje].predict(x)
    napoved2 = napovedni_modeli2[vprasanje].predict(x)
    
    napoved = [test.randomDelezi(vprasanje, delezi, napoved1[i], napoved2[i]) for i in range(0, len(napoved2))]
    
    return napoved


def napovejPrimer(vprasanje, odgovor, i):
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}
    
    t_vpr[vprasanje] = v
    
    X, y_ = napovedovanje.pridobiXY(vprasanje, t_vpr, {}, {}, vektorji[i].u1, modeli, "cossim")

    

    
    ocena = napovedni_modeli[i][vprasanje].predict(X)

    return ocena[0]
  

def testirajVsak():
    napovedni_mod = []
    maks_vr = []
    y = []
    vektorji = []
    
    #Iteriraj čez vse
    for i in range(0, len(vsi)):
        print(str(i) + " - ta iteracija...")
        t = vsi[i]
        uc = vsi[:i-1] + vsi[i:]
        y.append(t.ocena)
        
        u1_, u05_, u0_ = test.pretvoriVslovar(uc)
        
        u1_vekt = napovedovanje.vrniVektorje(u1_)
        vektorji.append(napovedovanje.Vektor())
        vektorji[i].u1 = u1_vekt
        u05_vekt = napovedovanje.vrniVektorje(u05_)
        vektorji[i].u05 = u05_vekt
        nm_, mv_ = nauciModele(u1_, u05_, u0_, u1_vekt, u05_vekt, modeli)
        
        napovedni_mod.append(nm_)
        maks_vr.append(mv_)
    
    return napovedni_mod, maks_vr, y, vektorji
        


