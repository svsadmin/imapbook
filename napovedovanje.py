# -*- coding: utf-8 -*-

"""
Created on Fri Jan 11 17:32:58 2019

@author: admin

"""
import sys
import nltk
import numpy
import operator
import collections
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet as wn
from gensim.corpora import Dictionary
from gensim.models import word2vec
from gensim.models.word2vec import Word2Vec
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import stopwords
import types
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import RidgeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier


class Vprasanje():
    
    def __init__(self, tip, vprasanje, kontekst):
        self.tip = tip
        self.vprasanje = vprasanje
        self.kontekst = kontekst
        self.odgovori = []
        self.ocena = -1
        
    def dodajOdgovore(self, vprasanje):
        for o in vprasanje.odgovori:
            self.odgovori.append(o)
        self.ocena = vprasanje.ocena

class Odgovor():
    def __init__(self, vektor, predelani, besedilo):
        self.vektor = vektor
        self.predelani = predelani
        self.besedilo = besedilo

class Modeli():
    def __init__(self):
        self.tf_idf = None
        self.W2Vmodel = None
        self.Idf_ = None

class Vektor():
    def __init__(self):
        self.u1 = None
        self.u05 = None
        self.u0 = None


stopW_star = ['this', 'that', 'there', 'be', 'those', 'to', 'of', 'for', 'because', 
         'have', 'when', 'from','they', 'them', 'he', 'she', '.',
         ',','?',';','as','if','the','and',"'s",'a','his','her','their','him',
         'it','’','do','!','in','into','i', 'but','with','what']

stopW = ['this', 'that', 'there', 'be', 'those', 'to', 'of', 'for', 'because', 
         'have', 'when', 'from','they', 'them', 'he', 'she', '.',
         ',','?',';','as','if','the','and',"'s",'a','his','her','their','him',
         'it','’','do','!','in','into','i', 'but','with','what', 'at', 'an', 'would', 'who']

#WNetSlovar = {'ADJ':'a', 'ADJ_SAT':'s', 'ADV':'r', 'NOUN':'n', 'VERB':'v'}
WNetSlovar = {'ADV':'r', 'NOUN':'n', 'VERB':'v'}

#W2Vmodel = KeyedVectors.load_word2vec_format(
#        'A:\ONJ\Word2Vec\GoogleNews-vectors-negative300.bin\GoogleNews-vectors-negative300.bin', binary=True)

def predelaj(text):
    
    tokens = nltk.word_tokenize(text)
    tokens = [t.lower() for t in tokens]
    pos_oznake = nltk.pos_tag(tokens, tagset='universal')
    #tokens = [t for t in nltk.word_tokenize(text) if t not in stopwords.words('english')]
    lemmatizer = WordNetLemmatizer()
    #leme = [(lemmatizer.lemmatize(pos[0], pos='v'), pos[1]) for pos in pos_oznake]
    #leme = [lemmatizer.lemmatize(pos[0], pos=WNetSlovar.get(pos[1], "v")) for pos in pos_oznake]
    leme = [lemmatizer.lemmatize(pos[0], pos="v") for pos in pos_oznake]
    #leme = [pos[0] for pos in pos_oznake]
    
    return leme

def predelajTerke(text):
    
    tokens = nltk.word_tokenize(text)
    tokens = [t.lower() for t in tokens]
    pos_oznake = nltk.pos_tag(tokens, tagset='universal')
    #tokens = [t for t in nltk.word_tokenize(text) if t not in stopwords.words('english')]
    lemmatizer = WordNetLemmatizer()
    #leme = [(lemmatizer.lemmatize(pos[0], pos='v'), pos[1]) for pos in pos_oznake]
    #leme = [lemmatizer.lemmatize(pos[0], pos=WNetSlovar.get(pos[1], "v")) for pos in pos_oznake]
    leme = [(lemmatizer.lemmatize(pos[0], pos="v"), pos[1]) for pos in pos_oznake]
    #leme = [(pos[0], pos[1]) for pos in pos_oznake]
    
    return leme

def vrniVektorje(odgovori):
    odgovori_ = {}
    
    for vpr in odgovori.keys():
        #Predelani odgovori
        c = [predelajTerke(o) for o in odgovori[vpr].odgovori]
        #Enotne besede
        c_ = [t[0] for te in c for t in te]
        sestevalnik = collections.Counter(c_)
        #Odstrani kratke besede
        zaOdstranit = predelaj(vpr)
        for z in zaOdstranit:
            try:
                sestevalnik.pop(z)
            except: pass
        for o in stopW:
            try:
                sestevalnik.pop(o)
            except: pass
        
        #f = [f[0] for f in sestevalnik.most_common(10)]
        #c1 = [o for o in c if o[0] in f]
        #print(c1)
        odgovori_[vpr] = Odgovor(sestevalnik.most_common(10), c, c_)
    
    return odgovori_

def primerjajVektorjeWordNet(vprasanje, odgovor, odgovori):
    try:
        o = odgovori[vprasanje]
        vP = [v[1]/len(o.predelani) for v in o.vektor]
        odgovor = predelaj(odgovor)
        sestevek = collections.Counter(odgovor)
        vO = [sestevek[v[0]] if v[0] in sestevek.keys() else 0 for v in o.vektor]
        
        for s in sestevek.keys():
            synsets = wn.synsets(s)
            lemmaNames = [l for ss in synsets for l in ss.lemma_names()]
            
            for l in lemmaNames:
                #Če je sopomenka v vektorju
                if l in o.vektor:
                    vO[o.vektor.index()] = 1
        
        return numpy.array(vP), numpy.array(vO)
    except:
        return []

def vrniVektorjeWordNet(odgovori):
    odgovori_ = {}
    
    for vpr in odgovori.keys():
        #Predelani odgovori
        c = [predelajTerke(o) for o in odgovori[vpr].odgovori]
        #Enotne besede
        c_ = [t[0] for te in c for t in te]
        sestevalnik = collections.Counter(c_)
        #Odstrani kratke besede
        zaOdstranit = predelaj(vpr)
        for z in zaOdstranit:
            try:
                sestevalnik.pop(z)
            except: pass
        for o in stopW:
            try:
                sestevalnik.pop(o)
            except: pass
        
        
        brisane = []
        s_sestevalnik = sorted(sestevalnik.items(), key=operator.itemgetter(1), reverse=True)
        
        for cc,st in s_sestevalnik:
            if cc not in brisane:
                synsets = wn.synsets(cc)
                lemmaNames = [l for ss in synsets for l in ss.lemma_names()]
            
                for l in lemmaNames:
                    #Briši
                    if l in sestevalnik.keys() and l != cc:
                        brisane.append((l, cc))
        
        for b in brisane:
            bris = sestevalnik.pop(b[0], 0)
            sestevalnik[b[1]] += bris
        
        odgovori_[vpr] = Odgovor(sestevalnik.most_common(10), c, c_)
    
    return odgovori_


def vrniVektorjeWordNetB(odgovoriA, idf, odgovori):
    odgovori_ = {}
    
    for vpr in odgovori.keys():
        zaOdstranit = stopW + predelaj(vpr)
        #Predelani odgovori s kontekstom
        odgovoriA[vpr].odgovori.append(odgovoriA[vpr].kontekst)
        c = [predelaj(o) for o in odgovoriA[vpr].odgovori]
        c = [o for o in c if o not in zaOdstranit]
        #Enotne besede
        c_ = [t for te in c for t in te]
        
        najIdf = []
        for t in c_:
            try:
                najIdf.append((t, idf[t]))
            except:
                pass
        
        najIdf.sort(key=operator.itemgetter(1), reverse = True)
        
        #Predelaj vse odgovore
        c = [predelajTerke(o) for o in odgovori[vpr].odgovori]
        #Enotne besede
        c_ = [t[0] for te in c for t in te]
        sestevalnik = collections.Counter(c_)
        #Odstrani
        zaOdstranit = predelaj(vpr)
        for z in zaOdstranit:
            try:
                sestevalnik.pop(z)
            except: pass
        for o in stopW:
            try:
                sestevalnik.pop(o)
            except: pass
        
        vc = [c[0] for c in sestevalnik.most_common(10)]
        najIdf = [n for n in najIdf if n[0] not in vc]
        
        sestev = sestevalnik.most_common(10) + najIdf
        
        odgovori_[vpr] = Odgovor(sestev, c, c_)
    
    return odgovori_


def vrniVektorjeWordNetA(odgovori, idf):
    odgovori_ = {}
    
    for vpr in odgovori.keys():
        zaOdstranit = stopW + predelaj(vpr)
        #Predelani odgovori s kontekstom
        odgovori[vpr].odgovori.append(odgovori[vpr].kontekst)
        c = [predelaj(o) for o in odgovori[vpr].odgovori]
        c = [o for o in c if o not in zaOdstranit]
        #Enotne besede
        c_ = [t for te in c for t in te]
        
        najIdf = []
        for t in c_:
            try:
                najIdf.append((t, idf[t]))
            except:
                pass
        
        najIdf.sort(key=operator.itemgetter(1), reverse = True)
        odgovori_[vpr] = Odgovor(najIdf, c, c_)
    
    return odgovori_

def vrniBesedisceA(ucni):
    besedisce = []
    
    for vpr in ucni.keys():
        besedisce.append(''.join(ucni[vpr].kontekst.split('…')))
        for o in ucni[vpr].odgovori:
            besedisce.append(o)
    
    return besedisce

def primerjajVektorje(vprasanje, odgovor, odgovori):
    try:
        o = odgovori[vprasanje]
        vP = [v[1]/len(o.predelani) for v in o.vektor]
        #vP = [1 for v in o.vektor]
        odgovor = predelaj(odgovor)
        sestevek = collections.Counter(odgovor)
        
        vO = [sestevek[v[0]] if v[0] in sestevek.keys() else 0 for v in o.vektor]
        return numpy.array(vP), numpy.array(vO)
    except:
        return []

def primerjajVektorjeW2V(vprasanje, odgovor, odgovori, modeli):
    o = odgovori[vprasanje]
    vP = [v[1]/len(o.predelani) for v in o.vektor]
    #vP = [1 for v in o.vektor]
    odgovor = predelaj(odgovor)
    #sestevek = collections.Counter(odgovor)
    
    vO = []
    for v in o.vektor:
        cs = []
        for o in odgovor:
            try:
                cs.append(cosine_similarity(modeli.W2Vmodel[o].reshape(1, -1), modeli.W2Vmodel[v[0]].reshape(1, -1)))
            except:
                if o == v:
                    cs.append(1)
                else:
                    cs.append(0)
        vO.append(max(cs))
        
    return numpy.array(vP), numpy.array(vO)

def vrniW2VSim(vprasanje, testniOdgovori, odgovori, modeli):
    try:
        vpr = testniOdgovori[vprasanje]
        W2VSim = []
        zaOdstranit = predelaj(vprasanje) + stopW
        
        for odg in vpr.odgovori:
            skupaj = []
            odgovor = predelaj(odg)
            for z in zaOdstranit:
                if z in odgovor:
                    odgovor.pop(odgovor.index(z))
                        
            o = odgovori[vprasanje]
            for v in o.vektor:
                for oo in odgovor:
                    try:
                        skupaj.append(cosine_similarity(modeli.W2Vmodel[oo].reshape(1, -1), modeli.W2Vmodel[v[0]].reshape(1, -1))[0][0])
                    except:
                        pass
            if len(skupaj) == 0:
                skupaj.append(0)
            W2VSim.append(sum(skupaj)/len(skupaj))
        return W2VSim
    except: return []

def vrniW2VNajSim(vprasanje, testniOdgovori, odgovori, modeli):
    #print(vprasanje)
    try:
        vpr = testniOdgovori[vprasanje]
        odgovor = odgovori[vprasanje]
        W2VNajSim = []
        zaOdstranit = predelaj(vprasanje) + stopW
        
        vP = [v[1] for v in odgovor.vektor]
        max_idf = max(vP)
        vP = [v/max_idf for v in vP]
        vP = numpy.array(vP)
        
        #print("Noter")
        
        for odg in vpr.odgovori:
            odg = [o for o in predelaj(odg) if o not in zaOdstranit]
            
            vO = []
            for v in odgovor.vektor:
                vO_ = [0]
                for o in odg:
                    try: 
                        vO_.append(cosine_similarity(modeli.W2Vmodel[o].reshape(1, -1), modeli.W2Vmodel[v[0]].reshape(1, -1))[0][0])
                    except:
                        vO_.append(0)
                vO.append(vO_)
            for o in range(0, len(vO)):
                if isinstance(vO[o], list):
                    max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                    vrsta = o
                    
                    ostal_v_vrsti = False
                    p = o
                    while(ostal_v_vrsti == False):
                        for p in range(o, len(vO)):
                            if (isinstance(vO[p], list) and (vO[p][max_t[1]] > max_t[0])):
                                max_t = (vO[p][max_t[1]], max_t[1])
                                vrsta = p
                        
                        #print(str(vrsta) + " " + str(max_t) + str(o) + str(ostal_v_vrsti))
                        vO[vrsta] = max_t[0]
                        for l in range(0, len(vO)):
                            if (isinstance(vO[l], list)):
                                vO[l][max_t[1]] = 0
                                
                        if vrsta == o:
                            ostal_v_vrsti = True
                            vO[o] = max_t[0]
                            
                        else:
                            max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                            vrsta = o

            vO = [vO[v] * vP[l] for v in range(0, len(vO))]
            vO = numpy.array(vO)
            
            #Odkomentiraj za metodo Kosinusna podobnost NajW2V
            W2VNajSim.append(cosine_similarity(vO.reshape(1, -1),vP.reshape(1, -1))[0][0])
            
            #Odkomentiraj za metodo Vsota NajW2V
            #W2VNajSim.append(sum(vO)/len(vO))
        return W2VNajSim
    except:
        #e = sys.exc_info()[0]
        #print(e)
        return []
    
    
def vrniPodobnostSKontekstomWordNet(vprasanje, testniOdgovori, odgovori, modeli):
    try:
        vpr = testniOdgovori[vprasanje]
        konj = predelaj(''.join(vpr.kontekst.split('…')))
        st = stopW + stopwords.words('english')
        
        kontekst = [(k, modeli.Idf_[k]) for k in konj if k.isalpha() and k not in st]
        kontekst.sort(key=operator.itemgetter(1), reverse = True)
        
        podobnostSKontekstom = []
        
        vK = numpy.array([k[1] for k in kontekst])
        
        for odg in vpr.odgovori:
            odg = [o for o in predelaj(odg) if o not in st]
            
            vO = []
            for k in kontekst:
                synsets = wn.synsets(k[0])
                synsets = []
                lemmaNames = [l for ss in synsets for l in ss.lemma_names()]
                
                vO_ = [0]
                for o in odg:
                    if o in lemmaNames:
                        vO_.append(1)
                    else:
                        try:
                            vO_.append(cosine_similarity(modeli.W2Vmodel[o].reshape(1, -1), modeli.W2Vmodel[k[0]].reshape(1, -1))[0][0])
                        except:
                            vO_.append(0)
                
                vO.append(vO_)
            
            for o in range(0, len(vO)):
                if isinstance(vO[o], list):
                    max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                    vrsta = o
                    
                    ostal_v_vrsti = False
                    p = o
                    while(ostal_v_vrsti == False):
                        for p in range(o, len(vO)):
                            if (isinstance(vO[p], list) and (vO[p][max_t[1]] > max_t[0])):
                                max_t = (vO[p][max_t[1]], max_t[1])
                                vrsta = p
                        
                        #print(str(vrsta) + " " + str(max_t) + str(o) + str(ostal_v_vrsti))
                        vO[vrsta] = max_t[0]
                        for l in range(0, len(vO)):
                            if (isinstance(vO[l], list)):
                                vO[l][max_t[1]] = 0
                                
                        if vrsta == o:
                            ostal_v_vrsti = True
                            vO[o] = max_t[0]
                            
                        else:
                            max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                            vrsta = o

            vO = numpy.array(vO)
            #print(vO)
            podobnostSKontekstom.append(cosine_similarity(vO.reshape(1, -1),vK.reshape(1, -1)))
        
        return podobnostSKontekstom
    except:
        return []

def vrniPodobnostSKontekstom(vprasanje, testniOdgovori, odgovori, modeli):
    try:
        vpr = testniOdgovori[vprasanje]
        konj = predelaj(''.join(vpr.kontekst.split('…')))
        st = stopW + stopwords.words('english')
        
        kontekst = [(k, modeli.Idf_[k]) for k in konj if k.isalpha() and k not in st]
        kontekst.sort(key=operator.itemgetter(1), reverse = True)
        
        podobnostSKontekstom = []
        
        vK = numpy.array([k[1] for k in kontekst])
        
        for odg in vpr.odgovori:
            odg = [o for o in predelaj(odg) if o not in st]
            
            vO = []
            for k in kontekst:
                vO_ = [0]
                for o in odg:
                    try: 
                        vO_.append(cosine_similarity(modeli.W2Vmodel[o].reshape(1, -1), modeli.W2Vmodel[k[0]].reshape(1, -1))[0][0])
                    except:
                        vO_.append(0)
                vO.append(vO_)
            
            for o in range(0, len(vO)):
                if isinstance(vO[o], list):
                    max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                    vrsta = o
                    
                    ostal_v_vrsti = False
                    p = o
                    while(ostal_v_vrsti == False):
                        for p in range(o, len(vO)):
                            if (isinstance(vO[p], list) and (vO[p][max_t[1]] > max_t[0])):
                                max_t = (vO[p][max_t[1]], max_t[1])
                                vrsta = p
                        
                        #print(str(vrsta) + " " + str(max_t) + str(o) + str(ostal_v_vrsti))
                        vO[vrsta] = max_t[0]
                        for l in range(0, len(vO)):
                            if (isinstance(vO[l], list)):
                                vO[l][max_t[1]] = 0
                                
                        if vrsta == o:
                            ostal_v_vrsti = True
                            vO[o] = max_t[0]
                            
                        else:
                            max_t = (max(vO[o]), vO[o].index(max(vO[o])))
                            vrsta = o

            vO = numpy.array(vO)
            #print(vO)
            podobnostSKontekstom.append(cosine_similarity(vO.reshape(1, -1),vK.reshape(1, -1)))
        
        return podobnostSKontekstom
    except:
        return []
    
def vrniCosSim(vprasanje, testniOdgovori, odgovori, modeli):
    try:
        vpr = testniOdgovori[vprasanje]
        cosSim = []
        for odg in vpr.odgovori:
            try:
                #vP, vO = primerjajVektorjeW2V(vpr.vprasanje, odg, odgovori, modeli)
                #vP, vO = primerjajVektorjeWordNet(vpr.vprasanje, odg, odgovori)
                vP, vO = primerjajVektorje(vpr.vprasanje, odg, odgovori)
                cosSim.append(cosine_similarity(vP.reshape(1, -1), vO.reshape(1, -1)))
            except:
                return []
        return cosSim
    except:
        return []

def vrniCosSimWordNet(vprasanje, testniOdgovori, odgovori, modeli):
    try:
        vpr = testniOdgovori[vprasanje]
        cosSim = []
        for odg in vpr.odgovori:
            try:
                #vP, vO = primerjajVektorjeW2V(vpr.vprasanje, odg, odgovori, modeli)
                vP, vO = primerjajVektorjeWordNet(vpr.vprasanje, odg, odgovori)
                #vP, vO = primerjajVektorje(vpr.vprasanje, odg, odgovori)
                cosSim.append(cosine_similarity(vP.reshape(1, -1), vO.reshape(1, -1)))
            except:
                return []
        return cosSim
    except:
        return []

def vrniDolzino(vprasanje, testniOdgovori, odgovori):
    try:
        vpr = testniOdgovori[vprasanje]
        dolzine = []
        
        for odg in vpr.odgovori:
            dolzine.append(len(odg))
        return dolzine
    except: return []   

   
def pridobiXY(vprasanje, testniOdgovori1, testniOdgovori05, testniOdgovori0, pravilniOdgovori, modeli, mera):
    v1 = []
    v05 = []
    v0 = []
    
    
    if (mera == "cossim"):
        v1 = vrniCosSim(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v05 = vrniCosSim(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v0 = vrniCosSim(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
    if (mera == "w2vsim"):
        v1 = vrniW2VSim(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v1 = [[v] for v in v1]
        v05 = vrniW2VSim(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v05 = [[v] for v in v05]
        v0 = vrniW2VSim(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
        v0 = [[v] for v in v0]
    if (mera == "w2vNajsim"):
        v1 = vrniW2VNajSim(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v1 = [[v] for v in v1]
        v05 = vrniW2VNajSim(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v05 = [[v] for v in v05]
        v0 = vrniW2VNajSim(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
        v0 = [[v] for v in v0]
    if (mera == "cosSimWnet"):
        v1 = vrniCosSimWordNet(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v05 = vrniCosSimWordNet(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v0 = vrniCosSimWordNet(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
    if (mera == "kontekst"):
        v1 = vrniPodobnostSKontekstom(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v1 = [[v] for v in v1]
        v05 = vrniPodobnostSKontekstom(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v05 = [[v] for v in v05]
        v0 = vrniPodobnostSKontekstom(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
        v0 = [[v] for v in v0]
    if (mera == "kontekstWnet"):
        v1 = vrniPodobnostSKontekstomWordNet(vprasanje, testniOdgovori1, pravilniOdgovori, modeli)
        v1 = [[v] for v in v1]
        v05 = vrniPodobnostSKontekstomWordNet(vprasanje, testniOdgovori05, pravilniOdgovori, modeli)
        v05 = [[v] for v in v05]
        v0 = vrniPodobnostSKontekstomWordNet(vprasanje, testniOdgovori0, pravilniOdgovori, modeli)
        v0 = [[v] for v in v0]
    if (mera == "dolzina"):
        v1 = vrniDolzino(vprasanje, testniOdgovori1, pravilniOdgovori)
        v1 = [[v] for v in v1]
        v05 = vrniDolzino(vprasanje, testniOdgovori05, pravilniOdgovori)
        v05 = [[v] for v in v05]
        v0 = vrniDolzino(vprasanje, testniOdgovori0, pravilniOdgovori)
        v0 = [[v] for v in v0]
    
    
    
    X = []
    y = []
    
    for v in v1:
        X.append(v[0])
        y.append(1)
    for v in v05:
        X.append(v[0])
        y.append(5)
    for v in v0:
        X.append(v[0])
        y.append(0)
        
    return numpy.array(X).reshape(-1, 1), y

def ustvariModele(ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, vektorji05):
    
    modeli = {}
    for vpr in vektorji1.keys():
        X, y = pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, "cossim")
        X2, y2 = pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, "kontekst")
        X2 = X2/max(X2)
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, "dolzina")
        X3 = X3/max(X3)
        X = numpy.concatenate((X, X3), axis=1)
        if vpr in vektorji05.keys():
            X1, y1 = pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji05, "cossim")
            X = numpy.concatenate((X, X1), axis=1)
        modeli[vpr] = randomForest(X, y)
    
    return modeli
        
def ridgeKlasifikator(X, y):
    clf = RidgeClassifier().fit(X, y)
    return clf

def naiveBayes(X, y):
    gnb = GaussianNB()
    gnb.fit(X, y)
    return gnb

def randomForest(X, y):
    clf = RandomForestClassifier(n_estimators=10)
    clf.fit(X, y)
    return clf