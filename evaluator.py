# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 13:38:34 2019

@author: admin
"""

import pickle
import napovedovanje
import numpy
import test

#Določi absolutno pot
pot = ""

def napovejPrimerA(vprasanje, odgovor):
    u1, u_vektorji, modeli = pickle.load( open( pot  + "\modelA.p", "rb" ) )
    
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}
    
    t_vpr[vprasanje] = v
    w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u_vektorji, modeli)
    
    napoved = [0 if x == 0 else 5 if x < 0.20 else 1 for x in w2vNajSim]
    
    return napoved[0]

#print(napovejPrimerA("How does Shiranna feel as the shuttle is taking off?", "Shiranna felt nervous"))
    
def napovejPrimerB(vprasanje, odgovor):
    n_modeli1, n_modeli2, maks_vrednosti, u1, u1_vektorji, u05_vektorji, delezi, modeli = pickle.load(open( pot + "\modelB.p", "rb" ))
    
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}

    t_vpr[vprasanje] = v

    cosSim = napovedovanje.vrniCosSim(vprasanje, t_vpr, u1_vektorji, modeli)
    X = numpy.array([cosSim]).reshape(-1, 1)

    kontekst = napovedovanje.vrniPodobnostSKontekstom(vprasanje, t_vpr, u1_vektorji, modeli)
    X2 = numpy.array([kontekst]).reshape(-1, 1)
    X2 = X2/maks_vrednosti["kontekst"]
    X = numpy.concatenate((X, X2), axis=1)

    w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u1_vektorji, modeli)
    X3 = numpy.array([w2vNajSim]).reshape(-1, 1)
    X3 = X3/maks_vrednosti["w2vNajsim"]
    X = numpy.concatenate((X, X3), axis=1)

    if vprasanje in u05_vektorji.keys():
        cosSim05 = napovedovanje.vrniCosSim(vprasanje, t_vpr, u05_vektorji, modeli)
        X1 = numpy.array([cosSim05]).reshape(-1, 1)
        X1 = X1/maks_vrednosti["cossim05"]
        X = numpy.concatenate((X, X1), axis=1)


    napoved1 = n_modeli1[vprasanje].predict(X)
    napoved2 = n_modeli2[vprasanje].predict(X)

    napoved = test.randomDelezi(vprasanje, delezi, napoved1[0], napoved2[0])

    return napoved


def napovejPrimerC(vprasanje, odgovor):
    n_modeli1, n_modeli2, maks_vrednosti, u1, u1_vektorji, u05_vektorji, delezi, modeli = pickle.load(open(pot + "\modelC.p", "rb" ))
    
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}

    t_vpr[vprasanje] = v

    cosSim = napovedovanje.vrniCosSimWordNet(vprasanje, t_vpr, u1_vektorji, modeli)
    X = numpy.array([cosSim]).reshape(-1, 1)

    kontekst = napovedovanje.vrniPodobnostSKontekstomWordNet(vprasanje, t_vpr, u1_vektorji, modeli)
    X2 = numpy.array([kontekst]).reshape(-1, 1)
    X2 = X2/maks_vrednosti["kontekst"]
    X = numpy.concatenate((X, X2), axis=1)

    w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u1_vektorji, modeli)
    X3 = numpy.array([w2vNajSim]).reshape(-1, 1)
    X3 = X3/maks_vrednosti["w2vNajsim"]
    X = numpy.concatenate((X, X3), axis=1)

    if vprasanje in u05_vektorji.keys():
        cosSim05 = napovedovanje.vrniCosSim(vprasanje, t_vpr, u05_vektorji, modeli)
        X1 = numpy.array([cosSim05]).reshape(-1, 1)
        X1 = X1/maks_vrednosti["cossim05"]
        X = numpy.concatenate((X, X1), axis=1)


    napoved1 = n_modeli1[vprasanje].predict(X)
    napoved2 = n_modeli2[vprasanje].predict(X)

    napoved = test.randomDelezi(vprasanje, delezi, napoved1[0], napoved2[0])

    return napoved