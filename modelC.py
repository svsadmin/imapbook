# -*- coding: utf-8 -*-
"""
Created on Sun Jan 13 21:36:17 2019

@author: admin
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 13:13:01 2019

@author: admin

"""

import pickle
import test
import napovedovanje
import gensim
import numpy
import matplotlib as plt
from gensim.models import KeyedVectors
from gensim.models.word2vec import Word2Vec
from sklearn.metrics import f1_score

#Kar je nalozeno iz modela A
vsi, delezi = test.nalozi(".\podatki\Weightless_dataset_train.csv")
u_, d = test.nalozi(".\podatki\Weightless_dataset_trainA.csv")

zgodba = test.naloziZgodbo(".\podatki\Weightless.txt")

u1A, u05A, u0A = test.pretvoriVslovar(u_)
#u1, u05, u0 = test.pretvoriVslovar(ucni)




besedisce = napovedovanje.vrniBesedisceA(u1A) + zgodba
besedisce = [napovedovanje.predelaj(b) for b in besedisce]
idf, tfidf_model = test.pridobiIdf([" ".join(b) for b in besedisce])

W2Vmodel = gensim.models.Word2Vec(
        besedisce,
        size=150,
        window=10,
        min_count=2,
        workers=10)
W2Vmodel.train(besedisce, total_examples=len(besedisce), epochs=10)

"""
Pri uporabi že naučenih Googlovih vektorjev modela Word2Vec odkomentirajte naslednjo vrstico 
ter namesto obstoječe poti dodajte lastno pot do datoteke GoogleNews-vectors-negative300.bin
"""
#W2Vmodel = KeyedVectors.load_word2vec_format(
#        'F:\OTROCI\O\INF\II\ONJ\GoogleNews-vectors-negative300.bin', binary=True)

modeli = napovedovanje.Modeli()
modeli.tf_idf = tfidf_model
modeli.W2Vmodel = W2Vmodel
modeli.Idf_ = idf

def nauciModele(ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, vektorji05, modeli):
    n_modeli1 = {}
    n_modeli2 = {}
    maks_vrednosti = {}
    
    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "cosSimWnet")
        X2, y2 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "kontekstWnet")
        maks_vrednosti["kontekst"] = max(X2)
        X2 = X2/max(X2)
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji1, modeli, "w2vNajsim")
        maks_vrednosti["w2vNajsim"] = max(X3)
        X3 = X3/max(X3)
        X = numpy.concatenate((X, X3), axis=1)
        
        if vpr in vektorji05.keys():
            X1, y1 = napovedovanje.pridobiXY(vpr, ucniOdgovori1, ucniOdgovori05, ucniOdgovori0, vektorji05, modeli, "cossim")
            X1 /= max(X1)
            maks_vrednosti["cossim05"] = max(X1)
            X = numpy.concatenate((X, X1), axis=1)
        
        n_modeli1[vpr] = napovedovanje.ridgeKlasifikator(X, y)
        n_modeli2[vpr] = napovedovanje.randomForest(X, y)
        
    return n_modeli1, n_modeli2, maks_vrednosti


def testiraj(testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, vektorji05, maks_vrednosti, modeli, n_modeli1, n_modeli2):
    napoved_ = []
    y_ = []
    rezultati = []
    
    f = plt.pyplot.figure()
    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "cosSimWnet")
        X2, y2 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "kontekstWnet")
        X2 = X2/maks_vrednosti["kontekst"]
        X = numpy.concatenate((X, X2), axis=1)
        X3, y3 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "w2vNajsim")
        X3 = X3/maks_vrednosti["w2vNajsim"]
        X = numpy.concatenate((X, X3), axis=1)
        
        if vpr in vektorji05.keys():
            X1, y1 = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji05, modeli, "cossim")
            X1 = X1/maks_vrednosti["cossim05"]
            X = numpy.concatenate((X, X1), axis=1)
        
        napoved1 = n_modeli1[vpr].predict(X)
        napoved2 = n_modeli2[vpr].predict(X)
        
        napoved = [test.randomDelezi(vpr, delezi, napoved1[i], napoved2[i]) for i in range(0, len(napoved2))]
        
        
        for i in range(0, len(y)):
            napoved_.append(napoved[i])
            y_.append(y[i])
        
        tocnost = [1 if y[i] == napoved[i] else 0 for i in range(0, len(y))]
        t = sum(tocnost)/len(tocnost)

        plt.pyplot.hist(X)
        
        
        tocnost = [1 if y[i] == napoved[i] else 0 for i in range(0, len(y))]
        t = sum(tocnost)/len(tocnost)
        fmakro = f1_score(y, napoved, average='macro')
        fmikro = f1_score(y, napoved, average='micro')
        
        rezultati.append((vpr, fmakro, fmikro))
    f.savefig("fC.pdf", bbox_inches='tight')
    
    return rezultati, napoved_, y_



#Vsi podatki
u1, u05, u0 = test.pretvoriVslovar(vsi)

vu1 = napovedovanje.vrniVektorje(u1)

vu05 = napovedovanje.vrniVektorje(u05)

n_modeli1, n_modeli2, maks_vrednosti = nauciModele(u1, u05, u0, vu1, vu05, modeli)

r, n, y = testiraj(u1, u05, u0, vu1, vu05, maks_vrednosti, modeli, n_modeli1, n_modeli2)

print(r)
print("F1 mikro: " + str(f1_score(y, n, average='micro')))
print("F1 makro: " + str(f1_score(y, n, average='macro')))

modeli.W2Vmodel = W2Vmodel

pickle.dump((n_modeli1, n_modeli2, maks_vrednosti, u1, vu1, vu05, delezi, modeli), open("modelC.p", "wb" ))



#napovedni_modeli = []
#nm, maks_vrednosti = nauciModele(u1, u05, u0, u1_vektorji, u05_vektorji, modeli)
#napovedni_modeli.append(nm)
"""
r_vse = []
f_mikro = []
f_makro = []

k = 10
for i in range(0, k):
    ucni, testni = test.k_kratno(vsi, 10, i)
    u1, u05, u0 = test.pretvoriVslovar(ucni)
    t1, t05, t0 = test.pretvoriVslovar(testni)

    vu1 = napovedovanje.vrniVektorje(u1)
    vu05 = napovedovanje.vrniVektorje(u05)

    n_modeli1, n_modeli2, maks_vrednosti = nauciModele(u1, u05, u0, vu1, vu05, modeli)

    r, n, y = testiraj(t1, t05, t0, vu1, vu05, maks_vrednosti, modeli, n_modeli1, n_modeli2)
    r_vse.append(r)

    print(f1_score(y, n, average='micro'))
    f_mikro.append(f1_score(y, n, average='micro'))

    print(f1_score(y, n, average='macro'))
    f_makro.append(f1_score(y, n, average='macro'))
"""
    
    
    
    
    