# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 17:32:58 2019

@author: admin
"""
import pickle
import test
import napovedovanje
import gensim
import matplotlib as plt
from sklearn.metrics import f1_score

ucni, d = test.nalozi(".\podatki\Weightless_dataset_trainA.csv")
u1, u05, u0 = test.pretvoriVslovar(ucni)

zgodba = test.naloziZgodbo(".\podatki\Weightless.txt")

testni, delezi = test.nalozi(".\podatki\Weightless_dataset_train.csv")
t1, t05, t0 = test.pretvoriVslovar(testni)

besedisce = napovedovanje.vrniBesedisceA(u1) + zgodba
besedisce = [napovedovanje.predelaj(b) for b in besedisce]
idf, tfidf_model = test.pridobiIdf([" ".join(b) for b in besedisce])

W2Vmodel = gensim.models.Word2Vec(
        besedisce,
        size=150,
        window=10,
        min_count=2,
        workers=10)
W2Vmodel.train(besedisce, total_examples=len(besedisce), epochs=10)

modeli = napovedovanje.Modeli()
modeli.tf_idf = tfidf_model
modeli.W2Vmodel = W2Vmodel
modeli.Idf_ = idf

u_vektorji = napovedovanje.vrniVektorjeWordNetA(u1, idf)

def napovejPrimerA(vprasanje, odgovor):
    v = napovedovanje.Vprasanje(u1[vprasanje].tip, vprasanje, u1[vprasanje].kontekst)
    v.odgovori.append(odgovor)
    t_vpr = {}
    
    t_vpr[vprasanje] = v
    w2vNajSim = napovedovanje.vrniW2VNajSim(vprasanje, t_vpr, u_vektorji, modeli)
    return w2vNajSim

def testiraj(testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli):
    napoved_ = []
    y_ = []
    rezultati = []
    f = plt.pyplot.figure()
    for vpr in vektorji1.keys():
        X, y = napovedovanje.pridobiXY(vpr, testniOdgovori1, testniOdgovori05, testniOdgovori0, vektorji1, modeli, "w2vNajsim")
        
        
        plt.pyplot.hist(X)
        napoved = [0 if x == 0 else 5 if x < 0.20 else 1 for x in X]
        
        for i in range(0, len(y)):
            napoved_.append(napoved[i])
            y_.append(y[i])
        
        tocnost = [1 if y[i] == napoved[i] else 0 for i in range(0, len(y))]
        t = sum(tocnost)/len(tocnost)
        fmakro = f1_score(y, napoved, average='macro')
        fmikro = f1_score(y, napoved, average='micro')
        
        rezultati.append((vpr, fmakro, fmikro))
    f.savefig("fA.pdf", bbox_inches='tight')
    
    return rezultati,  napoved_, y_

pickle.dump((u1, u_vektorji, modeli), open( "modelA.p", "wb" ))

r, n, y = testiraj(t1, t05, t0, u_vektorji, modeli)
print(r)
print("F1 makro: " + str(f1_score(y, n, average='macro')))
print("F1 mikro: " + str(f1_score(y, n, average='micro')))