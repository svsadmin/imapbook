Navodila

0. Za napovedovanje potrebujete že generirane modele A, B in C,
ki jih pridobite z zagonom skript modelA.py, modelB.py in modelC.py.
V primeru uporabe že naučenih Googlovih vektorjev nastavite pot v skripti modelC.py.

1. nastavite absolutno pot v datoteki evaluator.py
2. poženite onj-dummy-server.py

Knjižnice:
- pickle
- ntlk
- sklearn
