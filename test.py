# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 17:40:09 2019

@author: admin
"""
import csv
import codecs
from napovedovanje import Vprasanje, Odgovor
import random
from random import shuffle
import itertools
from sklearn.feature_extraction.text import TfidfVectorizer

#Naloži celotno zgodbo
def naloziZgodbo(datoteka):
    besedilo = []
    
    f = codecs.open(datoteka, encoding='utf-8')
    for l in f:
        besedilo.append(l)
    
    return besedilo

#Naloži vprašanja in vrni njihov seznam
def nalozi(datoteka):
    f_train = open(datoteka, "r", encoding='utf-8')
    reader = csv.reader(f_train, delimiter='"')
    next(reader)
    
    #Vprasanja
    vprasanja = []
    
    delezi = {}
    
    for l in reader:
        #Dodaj noovo vprasanje
        ocena = float(".".join(l[13].split(",")))
        vprasanje = Vprasanje(l[9], l[5], l[15])
        vprasanje.odgovori.append(l[7])
        vprasanje.ocena = ocena
        vprasanja.append(vprasanje)
        
        if (ocena == 1.0):
            try:
                delezi[l[5]][0] += 1
            except:
                delezi[l[5]] = [1,0,0]
        elif (ocena == 0.5):
            try:
                delezi[l[5]][1] += 1
            except:
                delezi[l[5]] = [0,1,0]
        elif (ocena == 0.0):
            try:
                delezi[l[5]][2] += 1
            except:
                delezi[l[5]] = [0,0,1]
    
    delezi = {key:[delezi[key][0]/sum(delezi[key]), delezi[key][1]/sum(delezi[key]), 
                   delezi[key][2]/sum(delezi[key])] for key in delezi.keys()}
    return vprasanja, delezi


def izracunajIA(datoteka):
    f_train = open(datoteka, "r", encoding='utf-8')
    reader = csv.reader(f_train, delimiter='"')
    next(reader)
    
    IA = {}
    
    for l in reader:
        #Dodaj noovo vprasanje
        ocena_skup = float(".".join(l[13].split(",")))
        ocenaA = float(".".join(l[11].split(",")))
        ocenaB = float(".".join(l[9].split(",")))
        vprasanje = l[5]
        
        if ocenaA == ocenaB:
            try:
                IA[vprasanje].append(1)
            except:
                IA[vprasanje] = []
                IA[vprasanje].append(1)
        else:
            try:
                IA[vprasanje].append(0)
            except:
                IA[vprasanje] = []
                IA[vprasanje].append(0)
    
    for ia in IA.keys():
        IA[ia] = sum(IA[ia])/len(IA[ia])
    
    return IA

#Pretvori seznam vprašanj v slovar pravilnih, napačnih in 0,5
def pretvoriVslovar(vprasanja):
    
    vpr_1 = {}
    vpr_05 = {}
    vpr_0 = {}
    
    for v in vprasanja:
        if (v.ocena == 1.0):
            try:
                vpr_1[v.vprasanje].dodajOdgovore(v)
            except:
                vpr_1[v.vprasanje] = Vprasanje(v.tip, v.vprasanje, v.kontekst)
                vpr_1[v.vprasanje].dodajOdgovore(v)
        
        elif (v.ocena == 0.5):
            try:
                vpr_05[v.vprasanje].dodajOdgovore(v)
            except:
                vpr_05[v.vprasanje] = Vprasanje(v.tip, v.vprasanje, v.kontekst)
                vpr_05[v.vprasanje].dodajOdgovore(v)
                
        elif (v.ocena == 0.0):
            try:
                vpr_0[v.vprasanje].dodajOdgovore(v)
            except:
                vpr_0[v.vprasanje] = Vprasanje(v.tip, v.vprasanje, v.kontekst)
                vpr_0[v.vprasanje].dodajOdgovore(v)
    
    return vpr_1, vpr_05, vpr_0


#Določi učno in testni množico
def delezTestnih(vprasanja, delez):
    
    shuffle(vprasanja)
    
    st_vrst = len(vprasanja)
    delitelj = int(st_vrst/((delez * st_vrst)/100))
    
    testna = [vprasanja[i] for i in range(0, st_vrst)  if (i%delitelj == 0)]
    ucna = [vprasanja[i] for i in range(0, st_vrst) if (i%delitelj != 0)]
    
    return ucna, testna

def randomDelezi(vprasanje, delezi, r1, r2):
    razmerja = delezi[vprasanje]
    sl = {0:2, 5:1, 1:0}
    slObrnjeno = {2:0, 1:5, 0:1}
    
    if r1 == r2:
        return r1
    

    else:
        prvi = razmerja[sl[r1]]
        drugi = razmerja[sl[r2]]
        celota = prvi + drugi
        
        r = random.uniform(0, celota)
        if (r <= prvi):
            return r1
        else: return r2   


def k_kratno(vsi, k, n):
    
    vsa = list(vsi)
    
    random.seed(32523)
    random.shuffle(vsa)
    
    dolzina = int(len(vsa)/k)
    ucna = []
    testna = []
    idx_k = dolzina
    
    idx_z = 0
    
    st = 0
    stu = 0
    for i in range(0, k):
        if (i == n):
            testna.append([])
            if (i == (k-1)):
                idx_k = len(vsa) - st
        
            for j in range(idx_z, idx_k):
                testna[0].append(vsa[st])
                st+=1
        else:
            ucna.append([])
            if (i == (k-1)):
                idx_k = len(vsa) - st
                
        
            for j in range(idx_z, idx_k):
                ucna[stu].append(vsa[st])
                st+=1
            stu+=1
        
    
    ucna = list(itertools.chain.from_iterable(ucna))
    testna = list(itertools.chain.from_iterable(testna))
    return ucna, testna

#Posamezno testiranje
def posameznoTestiranje(vprasanja, i):
    
    testno_vpr = vprasanja.pop(i)
    
    return vprasanja, testno_vpr
    
#Pridobi IDF   
def pridobiIdf(besedisce):
    tfIdf_cv = TfidfVectorizer()
    Idf_ = {}
    tfidf = tfIdf_cv.fit_transform(besedisce) 
    
    fnames = tfIdf_cv.get_feature_names()
    idf = tfIdf_cv.idf_
    
    for i in range(0, len(fnames)):
        Idf_[fnames[i]] = idf[i]
    
    return Idf_, tfIdf_cv


    
    
    